/**
 * @author Diana Soldan
 * @date : 4/6/2019
 */
public class Circle {
    public static double PI = 3.14;

    public static void showPI (){
        System.out.println(PI);
    }


    public double radius;

    public double calculateDiameter() {


        return 2 * radius;

    }

    public double calculateCircumference() {
        return 2 * PI * radius;

    }

    public double calculateArea() {
        return PI * radius * radius;
    }
}