import java.util.Scanner;

/**
 * @author Diana Soldan
 * @date : 4/6/2019
 */
public class MyMatrix {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int[][] ma = new int[3][4];

        // citire de valori
        for (int i = 0; i < ma.length; i++) {
            for (int j = 0; j < ma[i].length; j++) {
                System.out.print("introduceti valoare pentru " + i + "," + j + ": ");
                ma[i][j] = s.nextInt();
            }
        }

        // afsare matrice
        for (int i = 0; i < ma.length; i++) {
            for (int j = 0; j < ma[i].length; j++) {
                System.out.print(ma[i][j] + " ");
            }
            System.out.println();
        }

        int[][] mb = {{6, 7}, {3, 6, 4}, {9, 2}};

        for (int i = 0; i < mb.length; i++) {
            for (int j = 0; j < mb[i].length; j++) {
                System.out.print(mb[i][j] + " ");
            }
            System.out.println();
        }

        int[][] mc = new int[3][3];

    }
}
