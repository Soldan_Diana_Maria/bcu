/**
 * @author Diana Soldan
 * @date : 4/7/2019
 */
public class MyStringExample {
    public static void main(String[] args) {
        String s1 = new String();
        String s2 = new String("abc");
        String s3 = "abcd";

        char c = 'c';
        char c2 = '\n';
        char c3 = '\\';
        System.out.println(c);
        System.out.println(c2);
        System.out.println(c3);
        String someMessage = "Ana are mere";
        System.out.println(someMessage.length());

        System.out.println(someMessage.charAt(2));
        String emptyString= "";
        System.out.println(emptyString.isEmpty());

        System.out.println(someMessage.substring(3,6));
        someMessage.toUpperCase();
        System.out.println(someMessage.toUpperCase());

        System.out.println(someMessage + " si pere.");
        System.out.println(someMessage.concat(" si pere.") );
        someMessage= someMessage.concat(" si pere.");
        System.out.println(someMessage);
        someMessage=someMessage.replace(".", ",");
        System.out.println(someMessage);
        String [] split=someMessage.split("");
        for (int i=0; i< split.length;i++){
            System.out.println(split[i]);

            String m1= "abc'";
            String m2="abc";
            System.out.println(m1 == m2);

            m1= m1 + "d";
            m2= m2 + "d";
            System.out.println(m1 == m2);
            System.out.println(m1.equals(m2));
        }
    }
}
