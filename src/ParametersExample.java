/**
 * @author Diana Soldan
 * @date : 4/6/2019
 */
public class ParametersExample {

    public static void incrementValue(int n) {
        n++;

    }

    public static void incrementValue(Circle c) {
        System.out.println(c);
        if (c != null) {
            c.radius++;

        }

    }
    public static void main (String[]args){
        int n = 7;
        System.out.println(n);
        incrementValue(n);
        System.out.println(n);

        Circle c = new Circle();
        c.radius = 7;
        System.out.println(c.radius);
        incrementValue(c);
        System.out.println(c.radius);

        Circle c2 = null;
        System.out.println(c2);

    }
}
