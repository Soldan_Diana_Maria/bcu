/**
 * @author Diana Soldan
 * @date : 4/7/2019
 */
public class Square {
    public int side;

    public Square() {

    }

    public Square(int side) {
        this.side = side;
    }

}
