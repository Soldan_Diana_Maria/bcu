/**
 * @author Diana Soldan
 * @date : 4/7/2019
 */
public class ConstructorExample {
    public static void main(String[] args) {

        Square s = new Square(5);
        System.out.println(s.side);
        Square s1 = new Square();
        s1.side = 7;
        System.out.println(s1.side);
    }
}